# piboy-tester

Moved to the monorepo : https://gitlab.com/recalbox/recalbox/-/tree/master/projects/piboy/


~~~~~~~~~~~~



Simple utility to test if current case is a PiBoy DMG.

Should return 198 or 70 if case is a piboy
